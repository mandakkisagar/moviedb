import axios from 'axios';

const API_KEY = 'cfaa446e2e5ea8f0371b76bec86a453b';
const ROOT_URL = `https://api.themoviedb.org/3/search/multi?api_key=${API_KEY}&language=en-US&page=1`;

export const FETCH_MOVIES ='FETCH_MOVIES';

export function fetchMovies(movie){
  const url = `${ROOT_URL}&query=${movie}`;
  const request = axios.get(url);

  return{
    type: FETCH_MOVIES,
    payload:request
  }
}
