import React from 'react';
import SearchBar from '../containers/search_bar'
import MovieList from '../containers/movie_list'

export default () => {
  return (
    <nav className="navbar navbar-default">
      <div className="container-fluid col-md-10 col-md-offset-1">
          <SearchBar />
          <MovieList />
      </div>
    </nav>

  )
};
