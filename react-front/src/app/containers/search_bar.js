import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { fetchMovies } from '../actions/index';

class SearchBar extends Component{
  constructor(props){
    super(props);
    this.state = {
      term: ''
    }
  };

  onInputChange(event){
    this.setState({
      term:event.target.value
    })
  }

  onSubmit(event){
    event.preventDefault();
    this.props.fetchMovies(this.state.term);
    this.setState({
      term:''
    });

  }

  render(){
    return(
        <form onSubmit={this.onSubmit.bind(this)} id="search_bar" className="input-group">
          <div className="form-group">
            <input
              placeholder="Search for movies, actors, tv shows..."
              className="form-control"
              value={this.state.term}
              onChange={this.onInputChange.bind(this)}
              type="text"
              />
          </div>
          <span className="input-group-btn">
            <button type="submit" className="btn btn-default">SEARCH</button>
          </span>
        </form>
    )
  }
}
function mapDispatchToProps(dispatch) {
  return bindActionCreators({fetchMovies}, dispatch);
}
export default connect (null, mapDispatchToProps)(SearchBar);
