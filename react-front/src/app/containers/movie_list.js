import React, { Component } from 'react';
import { connect } from 'react-redux';

class MovieList extends Component{
  constructor(props){
    super(props);
    this.state = {

    };
  }
  renderMovie(movie){
    if (movie) {
      return (
        <span>{movie.original_title}<br /></span>
      )
    }
  }

  render(){
    if (this.props.movies.length > 0) {
      return(
        <div>
          {this.props.movies[0].map(this.renderMovie)}
        </div>
      )
    }else {
      return null
    }
  }
}

function mapStateToProps({movies}) {
  return {
    movies:movies
  };
}

export default connect(mapStateToProps)(MovieList);
